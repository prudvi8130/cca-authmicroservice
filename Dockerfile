#syntax=docker/dockerfile:1
FROM node:12
#Create app directory and copy package files

WORKDIR /usr/app-src
COPY package*.json ./

#Install app dependencies

RUN npm install

#Copy the app source

COPY . .

#Just for CCA
RUN echo "Creating a docker image by kalidindip2@udayton.edu"

CMD ["npm","start"]