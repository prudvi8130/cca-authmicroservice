const express = require('express')
const app = express()
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-kalidindip2:Cloud0804@cca-kalidindip2.lghqd.mongodb.net/cca-labs?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err => {
    if (err) throw err;
    console.log("Connected to the MongoDB cluster");
});                    
var port = process.env.PORT || 8080;
const cors = require('cors')
app.use(cors())
app.use(express.json());
let fields = {_id: false,
    zips: true,
    city: true,
    state_id: true,
    state_name: true,
    county_name: true,
    timezone: true};
app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
app.listen(port, () =>
     console.log(`HTTP Server with Express.js is listening on port:${port}`))
//app.get('/',(req, res) => {
//    res.sendFile(__dirname + '/static/cca-form.html');
//})      
app.get('/echo.php', function (req, res) {
    var data = req.query.data
    res.send(data)
})
app.get('/echo.php', function (req, res) {
    var data = req.query.data
    res.send(data)
})
app.post('/', function (req, res) {
    var data = req.body['data']
    res.send(data)
})
app.get('/uscities-search', (req, res)=> {
    res.send("US City search Microservice by Prudviraj kalidindi")
})
app.get('/uscities-search/:zips(\\d{1,5})', function (req,res) {
    const db = dbClient.db();
    let zipRegEx = new RegExp(req.params.zips);
    const cursor = db.collection("uscities").find({zips:zipRegEx}).project(fields);
    cursor.toArray(function(err, results){
        console.log(results);
        res.send(results);
    });
});
app.get('/uscities-search/:city', function (req,res){
    const db =dbClient.db();
    let cityRegEx = new RegExp(req.params.city,'i');
    const cursor = db.collection("uscities").find({city:cityRegEx}).project(fields);
    cursor.toArray(function(err, results){
        console.log(results);
        res.send(results);
    });    
});

app.get("/",(req,res)=>{
    res.sendFile(__dirname+'/static/chatclient/index.html')
})

app.post("/login", function(req,res){
    const db = dbClient.db();
    const {username,password} = req.body
    //some validations
    db.collection("users").findOne({username:username,password:password},(err, user)=>{
        if (user){
            user.status = true
            res.send(user) 
        }
        else{
            user = {}
            user.status = false
            res.send(user)
        }
    });
})
app.post("/signup", function(req,res){
    const db = dbClient.db();
    const {username,password,fullname,email} = req.body
    //some validations
    db.collection("users").findOne({username:username},(err, user)=>{
        if (user){
            user.signup = false 
            res.send(user) 
        }
        else{
            db.collection("users").insertOne(req.body,(err, user)=>{
                  user.signup = true
                  res.send(user)    
            })
        }
    });
})
